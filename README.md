# Unused DB Tables
Over the course of time, tables in our relational databases become unused or deprecated with little documentation.
This often manifests as tech debt that we'll get around to  *eventually*. 

This repo was developed as a quick way to identify these tables in the system based on access/usage,
and then search the codebase (in our case `Bitbucket`) for occurrences of those table names,
so we know where we need to refactor.

# Requirements
* Database credentials stored in the environment:
    * db name, host, port, username, password
* Bitbucket API credentials
    * team name, username, password
* External libraries
    * `psycopg2`, `requests`, `pprint` (for printing json nicely)
    
# Running the program
Just call main/main.py from src directory. 
You can easily change out db module, the table access query, or the API call depending on your version-control system.

