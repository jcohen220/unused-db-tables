-- Query used to determine which tables are infrequently accessed and potential candidates for removal.
SELECT
  relname
FROM
  pg_stat_user_tables
WHERE
  (idx_tup_fetch + seq_tup_read) < 30 --access threshold
  AND schemaname = 'public';