from os import environ
import requests

# The endpoint we need to hit to search our code in bitbucket
request_query = 'https://api.bitbucket.org/2.0/teams/{team}/search/code?search_query='.format(
        team=environ['BITBUCKET_TEAM'])


def get_payload(query, params):
    """
    Returns a json object with the payload from the code search request
    :param query: the url string-formatted above.
    :param params: the search parameters for the query
    :return: a json object with the request payload.
    """
    request = requests.get('{query}{search_params}'.format(query=query,
                                                           search_params=params),
                           auth=(environ['BITBUCKET_USER'],
                                 environ['BITBUCKET_PASSWORD']))

    return request.json()
