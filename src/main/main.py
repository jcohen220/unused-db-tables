"""
Driver program.
Queries the database for infrequently used tables,
and searches the codebase in Bitbucket for occurrences
of those table names, so we know if we need to refactor.
"""
import pprint
from pg_connection import get_connection
import bitbucket_request


if __name__ == '__main__':

    pp = pprint.PrettyPrinter()
    cur = None
    conn = None

    try:
        conn = get_connection()
        cur = conn.cursor()
        with open('./sql_queries/infrequent_table_access.sql', 'r') as f:
            query = f.read()
        cur.execute(query)
        raw_query_results = cur.fetchall()
    finally:
        cur.close()
        conn.close()

    # We just need the first tuple element (table name) second is just the schema.
    unused_tables = [tup[0] for tup in raw_query_results]

    request_query = bitbucket_request.request_query

    for table in unused_tables:
        payload = bitbucket_request.get_payload(request_query, table)
        print('payload for table = {table}\n'.format(table=table))
        pp.pprint(payload)  # print statement for now, TODO change to logger.


