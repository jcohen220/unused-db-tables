from os import environ
import psycopg2

# DB params and credentials read from environment
parameters = \
    {
    'user': environ['DB_USER'],
    'password': environ['DB_PASSWORD'],
    'host':environ['DB_HOST'],
    'port': environ['DB_PORT'],
    'database': environ['DB_DATABASE']
    }


def get_connection():
    """
    Instantiates a connection object for querying postgres.
    :return: an active connection object set from the env parameters.
    """
    return psycopg2.connect(**parameters)

